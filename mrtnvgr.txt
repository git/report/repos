{'description': {'en': "mrtnvgr's personal overlay"},
 'feed': ['https://github.com/mrtnvgr/gentoo-overlay/commits/master.atom'],
 'homepage': 'https://github.com/mrtnvgr/gentoo-overlay',
 'name': 'mrtnvgr',
 'owner': [{'email': 'martynovegorOF@yandex.ru',
            'name': 'Egor Martynov',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/mrtnvgr/gentoo-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/mrtnvgr/gentoo-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync mrtnvgr
*** syncing mrtnvgr
Already up to date.
*** synced mrtnvgr
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 mrtnvgr
 * Cache regenerated successfully
