{'description': {'en': "Kaa's experimental overlay"},
 'homepage': 'http://www.kaa.org.ua',
 'name': 'kaa',
 'owner': [{'email': 'oleg@kaa.org.ua',
            'name': 'Oleg Kravchenko',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/Oleh-Kravchenko/kaa-layman.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/Oleh-Kravchenko/kaa-layman.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync kaa
*** syncing kaa
Already up to date.
*** synced kaa
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 kaa
 * dev-libs/libsuinput-0.6.1-r2: failed sourcing ebuild: inherit requires unknown eclass: user.eclass
 * dev-util/pvs-studio-7.16.55312.179: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * media-video/tvtime-1.0.11-r3: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * Cache regen failed with 1
