{'description': {'en': 'Personal overlay of Haonan Chen'},
 'feed': ['https://github.com/CHN-beta/touchfish-os/commits/master.atom'],
 'homepage': 'https://github.com/CHN-beta/touchfish-os',
 'name': 'touchfish-os',
 'owner': [{'email': 'chn@chn.moe', 'name': 'Haonan Chen', 'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/CHN-beta/touchfish-os.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/CHN-beta/touchfish-os.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync touchfish-os
*** syncing touchfish-os
Already up to date.
*** synced touchfish-os
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 touchfish-os
 * Cache regenerated successfully
