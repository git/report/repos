{'description': {'en': 'a collection of ebuilds by mbaraa'},
 'feed': ['https://github.com/mbaraa/gentoo-overlay/commits/main.atom'],
 'homepage': 'https://github.com/mbaraa/gentoo-overlay',
 'name': 'mbaraa-overlay',
 'owner': [{'email': 'pub@mbaraa.com',
            'name': 'Baraa Al-Masri',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/mbaraa/gentoo-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/mbaraa/gentoo-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync mbaraa-overlay
*** syncing mbaraa-overlay
Already up to date.
*** synced mbaraa-overlay
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 mbaraa-overlay
 * Cache regenerated successfully
