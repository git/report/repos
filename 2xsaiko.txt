{'description': {'en': 'Personal overlay with no special focus'},
 'feed': ['https://git.sr.ht/~dblsaiko/ebuilds/log/rss.xml'],
 'homepage': 'https://git.sr.ht/~dblsaiko/ebuilds',
 'name': '2xsaiko',
 'owner': [{'email': 'me@dblsaiko.net',
            'name': 'Marco Rebhan',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git', 'uri': 'https://git.sr.ht/~dblsaiko/ebuilds'},
            {'type': 'git', 'uri': 'git@git.sr.ht:~dblsaiko/ebuilds'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync 2xsaiko
*** syncing 2xsaiko
Already up to date.
*** synced 2xsaiko
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
error: gpg.ssh.allowedSignersFile needs to be configured and exist for ssh signature verification
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 2xsaiko
 * Cache regenerated successfully
