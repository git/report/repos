{'description': {'en': 'Ebuilds related to game dev'},
 'feed': ['https://cgit.gentoo.org/repo/user/Drauthius.git/atom/'],
 'homepage': 'https://cgit.gentoo.org/repo/user/Drauthius.git/',
 'name': 'Drauthius',
 'owner': [{'email': 'albert@diserholt.com',
            'name': 'Albert Diserholt',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://anongit.gentoo.org/git/repo/user/Drauthius.git'},
            {'type': 'git',
             'uri': 'git://anongit.gentoo.org/repo/user/Drauthius'},
            {'type': 'git',
             'uri': 'git+ssh://git@git.gentoo.org/repo/user/Drauthius.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync Drauthius
*** syncing Drauthius
Already up to date.
*** synced Drauthius
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 Drauthius
 * Cache regenerated successfully
