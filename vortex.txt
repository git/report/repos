{'description': {'en': 'Personal overlay'},
 'feed': ['https://github.com/nE0sIghT/vortex-overlay/commits/master.atom'],
 'homepage': 'https://github.com/nE0sIghT/vortex-overlay',
 'name': 'vortex',
 'owner': [{'email': 'ykonotopov@gnome.org',
            'name': "Yuri 'nE0sIghT' Konotopov",
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/nE0sIghT/vortex-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/nE0sIghT/vortex-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync vortex
*** syncing vortex
Already up to date.
*** synced vortex
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 vortex
skipping repo vortex: cache disabled
 * Cache regenerated successfully
