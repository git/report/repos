{'description': {'en': "Official KDE team's testing overlay."},
 'feed': ['https://cgit.gentoo.org/proj/kde.git/atom/'],
 'homepage': 'https://kde.gentoo.org',
 'name': 'kde',
 'owner': [{'email': 'kde@gentoo.org', 'name': 'KDE Team', 'type': 'project'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://anongit.gentoo.org/git/proj/kde.git'},
            {'type': 'git', 'uri': 'https://github.com/gentoo/kde.git'},
            {'type': 'git', 'uri': 'git://anongit.gentoo.org/proj/kde.git'},
            {'type': 'git', 'uri': 'git+ssh://git@git.gentoo.org/proj/kde.git'},
            {'type': 'git', 'uri': 'git+ssh://git@github.com/gentoo/kde.git'}],
 'status': 'official'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync kde
*** syncing kde
Already up to date.
*** synced kde
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 kde
 * Cache regenerated successfully
