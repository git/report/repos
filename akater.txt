{'description': {'en': 'Personal ebuild repository.  Emacs, Lisp, minimalism.'},
 'feed': ['https://gitlab.com/akater/ebuilds/commits/master.atom'],
 'homepage': 'https://gitlab.com/akater/ebuilds',
 'name': 'akater',
 'owner': [{'email': 'nuclearspace@gmail.com',
            'name': 'akater',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git', 'uri': 'https://gitlab.com/akater/ebuilds.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@gitlab.com/akater/ebuilds.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync akater
*** syncing akater
Already up to date.
*** synced akater
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 akater
 * media-gfx/fim-0.5_rc3: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * sys-devel/gcc-10.2.0-r5: failed sourcing ebuild: rust: EAPI 7 not supported, (rust.eclass, line  48:  called die)
 * sys-devel/gcc-10.3.0: failed sourcing ebuild: rust: EAPI 7 not supported, (rust.eclass, line  48:  called die)
 * sys-devel/gcc-11.2.0: failed sourcing ebuild: rust: EAPI 7 not supported, (rust.eclass, line  48:  called die)
 * Cache regen failed with 1
