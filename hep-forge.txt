{'description': {'en': 'Inofficial HEPForge ebuilds'},
 'feed': ['https://gitlab.com/APN-Pucky/gentoo-hep-forge/commits/master.atom'],
 'homepage': 'https://gitlab.com/APN-Pucky/gentoo-hep-forge',
 'name': 'hep-forge',
 'owner': [{'email': 'alexander@neuwirth-informatik.de',
            'name': 'Alexander Puck Neuwirth',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://gitlab.com/APN-Pucky/gentoo-hep-forge.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@gitlab.com/APN-Pucky/gentoo-hep-forge.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync hep-forge
*** syncing hep-forge
Already up to date.
*** synced hep-forge
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 hep-forge
 * sci-physics/feynmodel: failed parsing metadata.xml: Document is empty, line 1, column 1 (metadata.xml, line 1)
 * sci-physics/pyqgraf: failed parsing metadata.xml: Document is empty, line 1, column 1 (metadata.xml, line 1)
 * Cache regen failed with 1
