{'description': {'en': 'Unofficial repository with all vapoursynth related '
                       'ebuilds'},
 'homepage': 'https://github.com/4re/vapoursynth-portage',
 'name': 'vapoursynth',
 'owner': [{'email': 'surukko@gmail.com', 'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/4re/vapoursynth-portage.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/4re/vapoursynth-portage.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync vapoursynth
*** syncing vapoursynth
Already up to date.
*** synced vapoursynth
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 vapoursynth
 * Cache regenerated successfully
