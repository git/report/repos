{'description': {'en': 'Personal overlay. Small programs and utils, X11\n'
                       '    themes, and version bumps of ebuilds from main '
                       'repo.'},
 'feed': ['https://github.com/Anonymous1157/sunset-repo/commits/master.atom'],
 'homepage': 'https://github.com/Anonymous1157/sunset-repo',
 'name': 'sunset-repo',
 'owner': [{'email': 'sunsetsergal@gmail.com', 'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/Anonymous1157/sunset-repo.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/Anonymous1157/sunset-repo.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync sunset-repo
*** syncing sunset-repo
Already up to date.
*** synced sunset-repo
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 sunset-repo
 * Cache regenerated successfully
