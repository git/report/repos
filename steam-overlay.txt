{'description': {'en': "Gentoo overlay for Valve's Steam client and "
                       'Steam-based games'},
 'feed': ['https://github.com/anyc/steam-overlay/commits/master.atom'],
 'homepage': 'https://github.com/anyc/steam-overlay',
 'name': 'steam-overlay',
 'owner': [{'email': 'dev@kicherer.org',
            'name': 'Mario Kicherer',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git', 'uri': 'https://github.com/anyc/steam-overlay.git'},
            {'type': 'git', 'uri': 'git@github.com:anyc/steam-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync steam-overlay
*** syncing steam-overlay
Already up to date.
*** synced steam-overlay
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 steam-overlay
 * Cache regenerated successfully
