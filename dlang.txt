{'description': {'en': 'A collection of ebuilds related to the D programming\n'
                       '    language.'},
 'feed': ['https://github.com/gentoo/dlang/commits/master.atom'],
 'homepage': 'https://github.com/gentoo/dlang',
 'name': 'dlang',
 'owner': [{'email': 'Marco.Leise@gmx.de', 'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git', 'uri': 'https://github.com/gentoo/dlang.git'},
            {'type': 'git', 'uri': 'git@github.com:gentoo/dlang.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync dlang
*** syncing dlang
Already up to date.
*** synced dlang
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 dlang
 * Cache regenerated successfully
