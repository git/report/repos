{'description': {'en': 'Developer overlay'},
 'feed': ['https://cgit.gentoo.org/dev/ago.git/atom/'],
 'homepage': 'https://cgit.gentoo.org/dev/ago.git/',
 'name': 'ago',
 'owner': [{'email': 'ago@gentoo.org',
            'name': 'Agostino Sarubbo',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://anongit.gentoo.org/git/dev/ago.git'},
            {'type': 'git', 'uri': 'git://anongit.gentoo.org/dev/ago.git'},
            {'type': 'git', 'uri': 'git+ssh://git@git.gentoo.org/dev/ago.git'}],
 'status': 'official'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync ago
*** syncing ago
Already up to date.
*** synced ago
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 ago
 * Cache regenerated successfully
