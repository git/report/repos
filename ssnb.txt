{'description': {'en': 'Gentoo repository made by ssnb'},
 'feed': ['https://cgit.gentoo.org/repo/user/ssnb.git/atom/'],
 'homepage': 'https://cgit.gentoo.org/repo/user/ssnb.git/',
 'name': 'ssnb',
 'owner': [{'email': 'samuelbernardo.mail@gmail.com',
            'name': 'Samuel Bernardo',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://anongit.gentoo.org/git/repo/user/ssnb.git'},
            {'type': 'git', 'uri': 'git://anongit.gentoo.org/repo/user/ssnb'},
            {'type': 'git',
             'uri': 'git+ssh://git@git.gentoo.org/repo/user/ssnb.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync ssnb
*** syncing ssnb
Already up to date.
*** synced ssnb
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 ssnb
 * Cache regenerated successfully
