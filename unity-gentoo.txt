{'description': {'en': 'Overlay to install the Unity desktop'},
 'feed': ['https://github.com/shiznix/unity-gentoo/commits/master.atom'],
 'homepage': 'https://github.com/shiznix/unity-gentoo',
 'name': 'unity-gentoo',
 'owner': [{'email': 'rickfharris@yahoo.com.au',
            'name': 'Rick Harris',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/shiznix/unity-gentoo.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/shiznix/unity-gentoo.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync unity-gentoo
*** syncing unity-gentoo
Already up to date.
*** synced unity-gentoo
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 unity-gentoo
 * dev-lang/vala-0.44.11: failed sourcing ebuild: gnome2: EAPI 6 not supported, (gnome2.eclass, line  16:  called die)
 * dev-libs/dee-1.2.7_p20170616_p6_p04: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * dev-libs/dee-1.2.7_p20170616_p6_p05: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * dev-libs/libcolumbus-1.1.0_p20150806_p0_p26-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * dev-libs/libcolumbus-1.1.0_p20150806_p0_p29: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * dev-libs/libindicator-16.10.0_p2018032101_p0_p05-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * dev-libs/libunity-7.1.4_p20190319_p6_p1-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * dev-libs/libunity-misc-4.0.5_p_p0_p03-r1: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * gnome-base/gconf-3.2.6_p_p07_p02-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * gnome-base/gnome-control-center-3.36.4: failed sourcing ebuild: No supported implementation in PYTHON_COMPAT., (python-utils-r1.eclass, line 168:  called die)
 * gnome-base/gnome-menus-3.36.0_p_p1_p3: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * gnome-base/gnome-panel-3.44.0_p_p1_p01: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * gnome-base/gnome-panel-3.46.0_p_p1_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * gnome-extra/activity-log-manager-0.9.7_p_p0_p28-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * gnome-extra/nm-applet-1.24.0_p_p1_p03: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * gnome-extra/nm-applet-1.28.0_p_p1_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * gnome-extra/polkit-gnome-0.105_p_p7_p03: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * mate-extra/mate-hud-22.04.4_p_p0_p01-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * mate-extra/mate-hud-22.10.3_p_p0_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * mate-extra/mate-menu-22.04.2_p_p0_p01-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * mate-extra/mate-tweak-22.04.8_p_p0_p03-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * mate-extra/mate-tweak-22.10.0_p_p0_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * mate-extra/ubuntu-mate-settings-22.04.14-r1: failed sourcing ebuild: toolchain-funcs: EAPI 6 not supported, (toolchain-funcs.eclass, line  21:  called die)
 * media-sound/rhythmbox-3.4.4_p_p5_p01-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * net-im/empathy-3.25.90_p_p2_p02-r1: failed sourcing ebuild: No supported implementation in PYTHON_COMPAT., (python-utils-r1.eclass, line 168:  called die)
 * net-libs/geonames-0.2_p20170220_p0_p04: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * net-libs/telepathy-indicator-0.3.1_p20140908_p0_p03: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * sys-apps/consolation-0.0.8_p1_p_p: failed sourcing ebuild: systemd: EAPI 6 not supported, (systemd.eclass, line  32:  called die)
 * sys-apps/xorg-gtest-0.7.1_p_p5_p01-r1: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * sys-kernel/ubuntu-sources-5.19.17_p_p29_p30: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * sys-power/cpufrequtils-008-r4: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-base/bamf-0.5.6_p20220217_p0_p01-r3: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-base/compiz-0.9.14.1_p20220820_p0_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-base/compiz-0.9.14.2_p20220822_p0_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-base/evemu-2.7.0_p3-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-base/frame-2.5.0_p20160809_p0_p03-r1: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-base/geis-2.2.17_p20160126_p0_p08-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-base/grail-3.1.1_p3: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-base/gsettings-ubuntu-touch-schemas-0.0.7_p20210712_p0_p02: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-base/nux-4.0.8_p20180623_p0_p04: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-base/overlay-scrollbar-0.2.17.1_p20151117_p0_p02: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-base/session-shortcuts-1.4-r1: failed sourcing ebuild: toolchain-funcs: EAPI 6 not supported, (toolchain-funcs.eclass, line  21:  called die)
 * unity-base/ubuntu-docs-16.04.4: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-base/unity-7.5.1_p2021102602_p0_p01-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-base/unity-7.6.0_p20220913_p0_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-base/unity-gtk-module-0.0.0_p20171202_p0_p03-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-base/unity-language-pack-22.04: failed sourcing ebuild: toolchain-funcs: EAPI 6 not supported, (toolchain-funcs.eclass, line  21:  called die)
 * unity-extra/indicator-evolution-0.2.20_p_p0_p42: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-extra/indicator-multiload-0.4_p_p0_p05: failed sourcing ebuild: toolchain-funcs: EAPI 6 not supported, (toolchain-funcs.eclass, line  21:  called die)
 * unity-extra/indicator-netspeed-9999: failed sourcing ebuild: git-r3: EAPI 6 not supported, (git-r3.eclass, line  30:  called die)
 * unity-extra/indicator-psensor-1.1.5_p_p0103_p02-r1: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-indicators/ido-13.10.0_p20161028_p0_p03-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-indicators/ido-13.10.0_p20221007_p0_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-indicators/indicator-applet-12.10.2_p20200915_p0_p01: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-indicators/indicator-application-12.10.1_p2019030801_p0_p03: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-indicators/indicator-appmenu-15.02.0_p2020061702_p0_p01: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-indicators/indicator-bluetooth-0.0.6_p20170605_p0_p03-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-indicators/indicator-keyboard-0.0.0_p20220803_p0_p1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-indicators/indicator-messages-13.10.1_p20180918_p0_p03-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-indicators/indicator-printers-0.1.7_p20171101_p0_p03: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-lenses/unity-lens-applications-7.1.0_p20160927_p0_p05-r3: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-lenses/unity-lens-files-7.1.0_p20170605_p0_p02-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-lenses/unity-lens-music-6.9.1_p_p0_p03-r3: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * unity-lenses/unity-lens-photos-1.0_p20170605_p0_p09-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-lenses/unity-lens-video-0.3.15_p2016021201_p0_p03-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-scopes/smart-scopes-7.5.1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * unity-scopes/unity-scope-home-6.8.2_p20190412_p0_p02-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-libs/gtk+-2.24.33_p_p2_p02-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-libs/gtk+-3.24.33_p_p1_p02-r1: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * x11-libs/gtk+-3.24.34_p_p3_p02: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-libs/topmenu-gtk-0.3_p1_p_p: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * x11-misc/gtk3-nocsd-3_p_p1_p01: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * x11-misc/gtk3-nocsd-3_p_p1_p02: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-misc/lightdm-1.30.0_p_p0_p05-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-misc/lightdm-1.30.0_p_p0_p07: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-misc/mate-dock-applet-21.10.0_p_p_p01-r2: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-misc/notify-osd-0.9.35_p20191129_p0_p02: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * x11-misc/vala-panel-0.5.0_p2-r1: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-misc/vala-panel-appmenu-0.7.6_p_p1_p04: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-plugins/pidgin-libnotify-0.14_p_p9_p02: failed sourcing ebuild: autotools: EAPI 6 not supported, (autotools.eclass, line  31:  called die)
 * x11-themes/humanity-icon-theme-0.6.16: failed sourcing ebuild: toolchain-funcs: EAPI 6 not supported, (toolchain-funcs.eclass, line  21:  called die)
 * x11-themes/ubuntu-sounds-0.14: failed sourcing ebuild: toolchain-funcs: EAPI 6 not supported, (toolchain-funcs.eclass, line  21:  called die)
 * x11-themes/ubuntu-themes-20.10_p_p0_p02: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-themes/ubuntu-wallpapers-22.04.4_p_p0_p01: failed sourcing ebuild: toolchain-funcs: EAPI 6 not supported, (toolchain-funcs.eclass, line  21:  called die)
 * x11-themes/unity-asset-pool-0.8.24_p20170507_p0_p03: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-wm/metacity-3.44.0_p_p_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * x11-wm/metacity-3.46.0_p_p_p01: failed sourcing ebuild: inherit requires unknown eclass: eutils.eclass
 * Cache regen failed with 1
