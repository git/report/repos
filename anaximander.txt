{'description': {'en': "anaximander's user overlay"},
 'feed': ['https://cgit.gentoo.org/user/anaximander.git/atom/'],
 'homepage': 'https://cgit.gentoo.org/user/anaximander.git/',
 'name': 'anaximander',
 'owner': [{'email': 'dominik.kriegner+gentoo@gmail.com',
            'name': 'Dominik Kriegner',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://anongit.gentoo.org/git/user/anaximander.git'},
            {'type': 'git',
             'uri': 'git://anongit.gentoo.org/user/anaximander.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@git.gentoo.org/user/anaximander.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync anaximander
*** syncing anaximander
Already up to date.
*** synced anaximander
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 anaximander
 * Cache regenerated successfully
