{'description': {'en': "mndz's Gentoo overlay"},
 'feed': ['https://github.com/0x6d6e647a/mndz-overlay/commits/master.atom'],
 'homepage': 'https://github.com/0x6d6e647a/mndz-overlay',
 'name': 'mndz',
 'owner': [{'email': '0x6d6e647a@gmail.com',
            'name': 'Anthony Mendez',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/0x6d6e647a/mndz-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com:0x6d6e647a/mndz-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync mndz
*** syncing mndz
Already up to date.
*** synced mndz
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 mndz
 * Cache regenerated successfully
