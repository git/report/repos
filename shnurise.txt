{'description': {'en': 'packages for mono environment and other',
                 'ru': 'пакеты для моно и разное прочее'},
 'feed': ['https://github.com/ArsenShnurkov/shnurise/commits/master.atom'],
 'homepage': 'https://github.com/ArsenShnurkov/shnurise',
 'name': 'shnurise',
 'owner': [{'email': 'Arsen.Shnurkov@gmail.com',
            'name': 'Arsen Shnurkov',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/ArsenShnurkov/shnurise.git'},
            {'type': 'git',
             'uri': 'git@github.com:ArsenShnurkov/shnurise.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync shnurise
*** syncing shnurise
Already up to date.
*** synced shnurise
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 shnurise
 * dev-dotnet/log4net-2.0.12: failed sourcing ebuild: inherit requires unknown eclass: mono.eclass
 * Cache regen failed with 1
