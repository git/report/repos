{'description': {'en': 'third party overlay to install overseerr'},
 'feed': ['https://github.com/chriscpritchard/overseerr-overlay/commits/main.atom'],
 'homepage': 'https://github.com/chriscpritchard/overseerr-overlay',
 'name': 'overseerr-overlay',
 'owner': [{'email': 'chris@christopherpritchard.co.uk',
            'name': 'Chris Pritchard',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/chriscpritchard/overseerr-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/chriscpritchard/overseerr-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync overseerr-overlay
*** syncing overseerr-overlay
Already up to date.
*** synced overseerr-overlay
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 overseerr-overlay
 * Cache regenerated successfully
