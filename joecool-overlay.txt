{'description': {'en': "joecool's personal overlay"},
 'feed': ['https://github.com/joecool1029/joecool-overlay/commits/master.atom'],
 'homepage': 'https://github.com/joecool1029/joecool-overlay',
 'name': 'joecool-overlay',
 'owner': [{'email': 'joe@wt.gd', 'name': 'Joe Kappus', 'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/joecool1029/joecool-overlay.git'},
            {'type': 'git',
             'uri': 'git@github.com:joecool1029/joecool-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync joecool-overlay
*** syncing joecool-overlay
Already up to date.
*** synced joecool-overlay
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 joecool-overlay
 * Cache regenerated successfully
