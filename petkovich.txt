{'description': {'en': 'Personal overlay of Jean-Christophe Petkovich'},
 'feed': ['https://github.com/jcpetkovich/overlay-petkovich/commits/master.atom'],
 'homepage': 'https://github.com/jcpetkovich/overlay-petkovich',
 'name': 'petkovich',
 'owner': [{'email': 'jcpetkovich@gmail.com',
            'name': 'Jean-Christophe Petkovich',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/jcpetkovich/overlay-petkovich.git'},
            {'type': 'git',
             'uri': 'git@github.com:jcpetkovich/overlay-petkovich.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync petkovich
*** syncing petkovich
Already up to date.
*** synced petkovich
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 petkovich
 * app-eselect/eselect-infinality-1-r1: failed sourcing ebuild: readme.gentoo-r1: EAPI 6 not supported, (readme.gentoo-r1.eclass, line  25:  called die)
 * sys-apps/cantools-0.17: failed sourcing ebuild: inherit: missing eclass argument
 * Cache regen failed with 1
