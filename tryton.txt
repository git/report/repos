{'description': {'en': 'Tryton'},
 'homepage': 'http://www.tryton.org/',
 'name': 'tryton',
 'owner': [{'email': 'cedk@gentoo.org', 'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'mercurial',
             'uri': 'https://foss.heptapod.net/tryton/gentoo-overlay'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync tryton
*** syncing tryton
pulling from https://foss.heptapod.net/tryton/gentoo-overlay
searching for changes
no changes found
*** synced tryton
 * Sync succeeded
$ hg log -l 1 --template={date|isodatesec}
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 tryton
 * Cache regenerated successfully
