{'description': {'en': "Han's personal ebuild repository"},
 'feed': ['https://github.com/hanmertens/han-overlay/commits/master.atom'],
 'homepage': 'https://github.com/hanmertens/han-overlay',
 'name': 'han',
 'owner': [{'email': 'hanmertens@outlook.com',
            'name': 'Han Mertens',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/hanmertens/han-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/hanmertens/han-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync han
*** syncing han
Already up to date.
*** synced han
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 han
 * Cache regenerated successfully
