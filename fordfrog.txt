{'description': {'en': 'Developer overlay'},
 'feed': ['https://cgit.gentoo.org/dev/fordfrog.git/atom/'],
 'homepage': 'https://cgit.gentoo.org/dev/fordfrog.git/',
 'name': 'fordfrog',
 'owner': [{'email': 'fordfrog@gentoo.org',
            'name': 'Miroslav Šulc',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://anongit.gentoo.org/git/dev/fordfrog.git'},
            {'type': 'git', 'uri': 'git://anongit.gentoo.org/dev/fordfrog.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@git.gentoo.org/dev/fordfrog.git'}],
 'status': 'official'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync fordfrog
*** syncing fordfrog
Already up to date.
*** synced fordfrog
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 fordfrog
 * Cache regenerated successfully
