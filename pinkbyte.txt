{'description': {'en': "Pinkbyte's developer overlay"},
 'feed': ['https://github.com/Pinkbyte/pinkbyte-overlay/commits/master.atom'],
 'homepage': 'https://github.com/Pinkbyte/pinkbyte-overlay',
 'name': 'pinkbyte',
 'owner': [{'email': 'pinkbyte@gentoo.org',
            'name': 'Sergey Popov',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/Pinkbyte/pinkbyte-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/Pinkbyte/pinkbyte-overlay.git'}],
 'status': 'official'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync pinkbyte
*** syncing pinkbyte
Already up to date.
*** synced pinkbyte
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 pinkbyte
 * Cache regenerated successfully
