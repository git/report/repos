{'description': {'en': "Overlay for software I didn't find in the main tree"},
 'feed': ['https://github.com/xxc3nsoredxx/unc3nsored/commits/master.atom'],
 'homepage': 'https://github.com/xxc3nsoredxx/unc3nsored',
 'name': 'unc3nsored',
 'owner': [{'email': 'xxc3ncoredxx@gmail.com',
            'name': 'Oskari Pirhonen',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/xxc3nsoredxx/unc3nsored.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/xxc3nsoredxx/unc3nsored.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync unc3nsored
*** syncing unc3nsored
Already up to date.
*** synced unc3nsored
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 unc3nsored
 * Cache regenerated successfully
