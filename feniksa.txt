{'description': {'en': 'Graphics, C++ libs and additional software'},
 'feed': ['https://github.com/feniksa/gentoo-overlay/commits/master.atom'],
 'homepage': 'https://github.com/feniksa/gentoo-overlay',
 'name': 'feniksa',
 'owner': [{'email': 'feniksa@200volts.com',
            'name': 'Maksym Sditanov',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/feniksa/gentoo-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/feniksa/gentoo-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync feniksa
*** syncing feniksa
Already up to date.
*** synced feniksa
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 feniksa
 * Cache regenerated successfully
