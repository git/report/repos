{'description': {'en': "rimio's personal overlay"},
 'feed': ['https://github.com/rimio/gentoo-overlay/commits/master.atom'],
 'homepage': 'https://github.com/rimio/gentoo-overlay',
 'name': 'rimio',
 'owner': [{'email': 'vasi@vilvoiu.ro',
            'name': 'Vasile Vilvoiu',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/rimio/gentoo-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/rimio/gentoo-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync rimio
*** syncing rimio
Already up to date.
*** synced rimio
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 rimio
 * Cache regenerated successfully
