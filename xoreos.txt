{'description': {'en': 'Gentoo overlay for the xoreos project, providing a WIP '
                       'reimplementation of the BioWare Aurora engine and '
                       'related modding tools.'},
 'feed': ['https://github.com/xoreos/gentoo-overlay/commits/master.atom'],
 'homepage': 'https://github.com/xoreos/gentoo-overlay',
 'name': 'xoreos',
 'owner': [{'email': 'drmccoy@drmccoy.de',
            'name': 'Sven "DrMcCoy" Hesse',
            'type': 'person'}],
 'quality': 'experimental',
 'source': [{'type': 'git',
             'uri': 'https://github.com/xoreos/gentoo-overlay.git'},
            {'type': 'git',
             'uri': 'git+ssh://git@github.com/xoreos/gentoo-overlay.git'}],
 'status': 'unofficial'}
pkgcore 0.12.28

$ pmaint --config /var/lib/repo-mirror-ci/data-sync/etc/portage sync xoreos
*** syncing xoreos
Already up to date.
*** synced xoreos
 * Sync succeeded
$ git log --format=%ci -1
$ git show -q --pretty=format:%G? HEAD
$ pmaint --config /var/lib/repo-mirror-ci/data/etc/portage regen --use-local-desc --pkg-desc-index -t 32 xoreos
 * Cache regenerated successfully
